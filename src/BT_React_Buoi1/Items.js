import React, { Component } from "react";

export default class Items extends Component {
  render() {
    return (
      <div class="col-lg-3 col-xxl-3 mb-5">
        <div class="card bg-light border-0 h-100">
          <img
            src={"./logo192.png"}
            style={{ height: "50%", margin: "auto", width: "50%" }}
          />
          <div class="card-body text-center p-4 mb-3 p-lg-5 pt-0 pt-lg-0">
            <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">
              <i class="bi bi-collection"></i>
            </div>
            <h2 class="fs-4 fw-bold">Fresh new layout</h2>
            <p class="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
          <div className="d-flex justify-content-center border-top p-3">
            <button className="btn btn-primary">Find Out More</button>
          </div>
        </div>
      </div>
    );
  }
}
